import java.util.Arrays;
import java.util.HashSet;

public class TestTechJDI {

    public static int solution(int[] A, int[] B) {
        int n = A.length;
        int m = B.length;

        Arrays.sort(A);
        Arrays.sort(B);
        int i = 0;
        for (int k = 0; k < n; k++) {
//            if(B[i] > A[k])
//                return -1;
            if (i < m && B[i] < A[k]) {
                i += 1;
                k--;
                continue;
            }
            if (A[k] == B[i])
                return A[k];
        }
        return -1;
    }


    //in ra hinh chu L
    public static void solution1(int N) {
        for (int i = 0; i < N; i++) {
            if (i == N - 1) {
                for (int j = 0; j < N; j++) {
                    System.out.print('L');
                }
                return;
            }
            System.out.println('L');
        }

    }

    //Tim tap hop khoang cach chia het cho M
    public static int solution2(int[] A, int M) {
        int count = 1;

        HashSet<Integer> hashSet = new HashSet<>();
        for (int i = 0; i < A.length - 1; i++) {
            if (hashSet.contains(i)) {
                continue;
            }

            int st = A[i];
            int temp = 1;
            for (int j = i + 1; j < A.length; j++) {
                if (Math.abs(A[j] - st) % M == 0) {
                    temp++;
                    hashSet.add(j);
                }
            }
            count = Math.max(count, temp);
        }
        return count;
    }

    //xac dinh diem (x,y) nam ben trong hinh tron nao
    //(x2 - x1)^2 + (y2 - y1)^2 = R^2
    public static int solution(int[] A, int[] B, int X, int Y) {
        int R = 20;
        for (int i = 0; i < A.length; i++) {
            if ((X - A[i]) * (X - A[i]) + (Y - B[i]) * (Y - B[i]) <= R * R) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
//        solution(4);
        int arr[] = {7, 1, 11, 8, 4, 10};
//        System.out.println(solution(arr, 8));

        int A[] = {4, 2, 5, 3, 2};
        int B[] = {1, 3, 2, 1};
        System.out.println(solution(B, A));


    }
}
