import java.util.*;

public class Al {

    public static class Tree {
        public int x;
        public Tree l;
        public Tree r;

        public Tree(int x, Tree l, Tree r) {
            this.x = x;
            this.l = l;
            this.r = r;
        }
    }

    public static int solution2(Tree T) {

        Map<Integer, Boolean> map = new HashMap();
        return recursive(T, map);

    }

    public static int recursive(Tree T, Map map) {
        int ls = map.size();
        int rs = map.size();

        System.out.println(map);

        if (map.get(T.x) == null || map.get(T.x) != null) {
            return map.size();
        }

        map.put(T.x, true);

        if (T.l != null) {
            ls = recursive(T.l, map);
        }

        if (T.r != null) {
            rs = recursive(T.r, map);
        }

        return Math.max(ls, rs);
    }


    public static int getDisCnt(Tree root) {
        Set<Integer> uniq = new HashSet<>();
        if (root == null) {
            return 0;
        }
        return getMaxHelper(root, uniq);
    }

    private static int getMaxHelper(Tree root, Set<Integer> uniq) {
        if (root == null) {
            return uniq.size();
        }
        int l = 0;
        int r = 0;
        if (uniq.add(root.x)) {
            l = getMaxHelper(root.l, uniq);
            r = getMaxHelper(root.r, uniq);
            uniq.remove(uniq.size() - 1);
        } else {
            l = getMaxHelper(root.l, uniq);
            r = getMaxHelper(root.r, uniq);
        }
        return Math.max(l, r);
    }

    public static int solution(int[] A) {
        int count = 0;

        for (int i = 0; i < A.length - 1; i++) {
            if (A[i] == A[i + 1]) {
                A[i + 1] = A[i + 1] % 1;
                count++;
            }
        }

        return (A.length - count < count) ? A.length - count : count;

    }

    public static int solution3(int[] T) {
        int count = 0;
        int max = T[0];

        for (int i = 0; i < T.length; i++) {
            if (checkGreaterThan(T, max, i + 1)) {
                return i + 1;
            }
            max = Math.max(max, T[i]);
        }
        return count;
    }

    public static boolean checkGreaterThan(int[] T, int max, int s) {
        for (int i = s; i < T.length; i++) {
            if (T[i] <= max) {
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        int A[] = {};
        System.out.println(solution(A));

        Tree root = new Tree(1, null, new Tree(2, new Tree(1, null, null), new Tree(1, new Tree(4, null, null), null)));
//        = new Tree(1, null, new Tree(2, new Tree(1, null, null), new Tree(1, new Tree(4, null, null), null)));
//        System.out.println(solution2(root));
//        int[] A = {-1, 4, 5, -6, 7, 8, 4, -1, 6, 10, 9, 8};
//        System.out.println(solution3(A));
    }

}
