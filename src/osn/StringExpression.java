package osn;

import java.util.Map;

public class StringExpression {
    public static String execute(String str) {
        final Map<String, Integer> DIGITS = Map.of(
                "zero", 0,
                "one", 1,
                "two", 2,
                "three", 3,
                "four", 4,
                "five", 5,
                "six", 6,
                "seven", 7,
                "eight", 8,
                "nine", 9
        );

        String word = "";
        int num = 0;
        int res = 0;
        String ope = "plus";

        for (int i = 0; i < str.length(); i++) {
            word += str.charAt(i);

            if (DIGITS.containsKey(word)) {
                num = num * 10 + DIGITS.get(word);
                word = "";
            } else if (str.charAt(i) == 'p') {
                res = calculate(res, ope, num); // calculate with old ope
                num = 0;
                word = "";
                ope = "plus";
                i += 3;
            } else if (str.charAt(i) == 'm') {
                res = calculate(res, ope, num); // calculate with old ope
                num = 0;
                word = "";
                ope = "minus";
                i += 4;
            }
        }

        res = calculate(res, ope, num);

        return convertNumberToString(res);
    }

    private static String convertNumberToString(int number) {
        String[] words = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

        if (number < 0) {
            return "negative" + convertNumberToString(-number);
        }

        if (number < 10) {
            return words[number];
        }

        return convertNumberToString(number / 10) + words[number % 10];
    }

    private static int calculate(int res, String ope, int num) {
        if (ope.equals("plus")) {
            return res + num;
        }
        return res - num;
    }
}
