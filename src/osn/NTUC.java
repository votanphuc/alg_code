package osn;

import java.util.HashSet;
import java.util.Set;

public class NTUC {

    public static void twoSum(int[] arr) {
        int n = arr[0];
        Set<Integer> set = new HashSet<>();

        for (int i = 1; i < arr.length; i++) {
            int opp = n - arr[i];
            if (set.contains(opp)) {
                System.out.printf("[%d, %d]", opp, arr[i]);
                set.remove(opp);
            } else {
                set.add(arr[i]);
            }
        }
    }

    public static void main(String[] args) {
//        LRUCache.add('A', "1");
//        LRUCache.add('B', "2");
//        LRUCache.add('C', "3");
//        LRUCache.add('D', "4");
//        LRUCache.add('A', "1");
//        LRUCache.add('E', "6");
//        LRUCache.add('D', "4");
//        LRUCache.add('Z', "8");
//
//        System.out.println(LRUCache.getCache());

//        System.out.println(StringExpression.execute("oneplustwoplusthreeplusfourplusthreefourminusnineninepluseightsixseven"));
        twoSum(new int[] {7, 3, 5, 2, -4, 8, 11});
    }
}
