package osn;

import java.util.LinkedHashMap;

public class LRUCache {

    private static final int MAX_SIZE = 5;
    private static final LinkedHashMap<Character, String> cache = new LinkedHashMap<>();

    public static LinkedHashMap<Character, String> getCache() {
        return cache;
    }

    public static void add(Character k, String v) {
        if (cache.containsKey(k)) {
            cache.remove(k);
        } else  if (cache.size() >= MAX_SIZE) {
            cache.remove(cache.keySet().iterator().next());
        }

        cache.put(k, v);
    }

    public static String get(Character k) {
        if (!cache.containsKey(k)){
            return "!!Error";
            //or throw exception
        }

        // updateCache(k)
        return cache.get(k);
    }

    private void updateCache(Character k) {
        String v = cache.get(k);
        cache.remove(k);
        cache.put(k, v);
    }
}
