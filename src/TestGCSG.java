import java.util.ArrayList;
import java.util.List;

public class TestGCSG {
    public static int maxStreak(int m, List<String> data) {
        int currentStreak = 0;
        int maxStreak = 0;
        for (String day : data) {
            for (int i = 0; i < m; i++) {
                if (day.charAt(i) != 'Y') {
                    currentStreak = 0;
                    break;
                }
                if (i == m - 1) {
                    currentStreak++;
                    maxStreak = Math.max(maxStreak, currentStreak);
                }
            }
        }

        return maxStreak;

    }

    public static void test() {
        try {
            Float f1 = new Float("3.0");
            int x = f1.intValue();
            byte b = f1.byteValue();
            double d = f1.doubleValue();
            System.out.println(x);
            System.out.println(b);
            System.out.println(d);
            System.out.println(x + b + d);
        } catch (NumberFormatException e) /* Line 9 */ {
            System.out.println("bad number"); /* Line 11 */
        }
    }

    public static void foo(boolean a, boolean b) {
        if (a) {
            System.out.println("A");
        } else if (a && b) {
            System.out.println("A && B");
        } else {
            if (!b) {
                System.out.println("notB");
            } else {
                System.out.println("ELSE");
            }
        }
    }

    public static void main(String[] args) {
        int m = 2;
        List<String> data = new ArrayList<>();
        data.add("NN");
        data.add("YY");
        data.add("NN");
        data.add("YY");
        data.add("YY");
        data.add("YN");
//        System.out.println(maxStreak(m, data));
//        test();
        foo(true, true);
        foo(true, false);
        foo(false, true);
        foo(false, false);
    }
}
