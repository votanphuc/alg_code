package code032023;

import java.util.ArrayList;
import java.util.List;

public class FindTotalInArray {
    public static void findSubsetsEqualTo15(int[] arr) {
        List<List<Integer>> subsets = new ArrayList<>();
        generateSubsets(arr, 0, new ArrayList<>(), subsets);
        for (List<Integer> subset : subsets) {
            int sum = 0;
            for (int element : subset) {
                sum += element;
            }
            if (sum == 15) {
                System.out.println(subset);
            }
        }
    }

    public static void generateSubsets(int[] arr, int index, List<Integer> current, List<List<Integer>> subsets) {
        if (index == arr.length) {
            subsets.add(new ArrayList<>(current));
            return;
        }
        // include current element
        current.add(arr[index]);
        System.out.println("time flies: " + current + "\nsubsets: " + subsets);
        generateSubsets(arr, index + 1, current, subsets);
        current.remove(current.size() - 1);
        // exclude current element
        generateSubsets(arr, index + 1, current, subsets);
    }

    public static List<Integer> findSubsetEqualTo15(int[] arr) {
        return findSubsetEqualTo15Helper(arr, 0, 0, new ArrayList<Integer>());
    }

    public static List<Integer> findSubsetEqualTo15Helper(int[] arr, int index, int sum, List<Integer> current) {
        if (sum == 15) {
            return current;
        }
        if (index == arr.length) {
            return null;
        }
        // include current element
        current.add(arr[index]);
        List<Integer> subset = findSubsetEqualTo15Helper(arr, index + 1, sum + arr[index], current);
        if (subset != null) {
            return subset;
        }
        current.remove(current.size() - 1);
        // exclude current element
        subset = findSubsetEqualTo15Helper(arr, index + 1, sum, current);
        if (subset != null) {
            return subset;
        }
        return null;
    }

    public static void main(String[] args) {
//        System.out.println(findSubsetEqualTo15(new int[]{1, 12, 7, 13, 7, 9}));
        findSubsetsEqualTo15(new int[]{1, 12, 7, 13, 7, 9});
    }
}
