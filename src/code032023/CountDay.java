package code032023;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

/**
 * Given a date say (05-05-2015) calculate the no. of days from that date to today without using libraries.
 */
public class CountDay {

    private int solution(String date) {
        String[] datePart = date.split("-");
        int day = Integer.parseInt(datePart[0]);
        int month = Integer.parseInt(datePart[1]);
        int year = Integer.parseInt(datePart[2]);

        LocalDate cDate = LocalDate.now();
        int cDay = cDate.getDayOfMonth();
        int cMonth = cDate.getMonthValue();
        int cYear = cDate.getYear();

        if (year == cYear) {
            return countDayOfYear(cDay, cMonth, cYear) - countDayOfYear(day, month, year);
        }

        if (year > cYear) {
            return -1;
        }

        int totalDays = (year - 1) * 365 + calculateNumberOfLeap(year - 1) + countDayOfYear(day, month, year);
        int totalCDays = (cYear - 1) * 365 + calculateNumberOfLeap(cYear - 1) + countDayOfYear(cDay, cMonth, cYear);
        return totalCDays - totalDays;
    }

    private int countDayOfYear(int day, int month, int year) {
        int count = 0;
        if (month > 2 && isLeapYear(year)) {
            count++;
        }

        switch (month) {
            case 2:
                count += 31;
                break;
            case 3:
                count += (31 + 28);
                break;
            case 4:
                count += (31 + 28 + 31);
                break;
            case 5:
                count += (31 + 28 + 31 + 30);
                break;
            case 6:
                count += (31 + 28 + 31 + 30 + 31);
                break;
            case 7:
                count += (31 + 28 + 31 + 30 + 31 + 30);
                break;
            case 8:
                count += (31 + 28 + 31 + 30 + 31 + 30 + 31);
                break;
            case 9:
                count += (31 + 28 + 31 + 30 + 31 + 30 + 31 + 31);
                break;
            case 10:
                count += (31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30);
                break;
            case 11:
                count += (31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31);
                break;
            case 12:
                count += (31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30);
                break;
        }
        return count + day;
    }

    private boolean isLeapYear(int year) {
        return (year % 4 == 0) && (year % 100 != 0) || year % 400 == 0;
    }

    private int calculateNumberOfLeap(int year) {
        return year / 4 - year / 100 + year / 400;
    }

    private int calculateNumberOfLeap(int start, int end) {
        return calculateNumberOfLeap(end - 1) - calculateNumberOfLeap(start + 1);
    }

    private int calculateNumberOfLeap2(int start, int end) {
        int count = 0;
        start++;
        while (start % 4 != 0) {
            start++;
        }

        while (start < end) {
            if (isLeapYear(start)) {
                count++;
            }
            start += 4;
        }
        return count;
    }

    public static void main(String[] args) {
        CountDay s = new CountDay();
        s.check();

        int day = 17;
        int month = 3;
        int year = 1000;
        System.out.println(s.solution(String.format("%s-%s-%s", day, month, year)));

        LocalDateTime date1 = LocalDate.of(year, month, day).atStartOfDay();
        LocalDateTime date2 = LocalDate.now().atStartOfDay();

        System.out.println(Duration.between(date1, date2).toDays());

    }

    public void check() {
        LocalDate date = LocalDate.now();
        System.out.println("countDayOfYear: " + (date.getDayOfYear() == countDayOfYear(date.getDayOfMonth(), date.getMonthValue(), date.getYear())));
        System.out.println("isLeapYear: " + (date.isLeapYear() == isLeapYear(date.getYear())));
        date = date.minusDays(100);
        System.out.println("countDayOfYear: " + (date.getDayOfYear() == countDayOfYear(date.getDayOfMonth(), date.getMonthValue(), date.getYear())));
        System.out.println("isLeapYear: " + (date.isLeapYear() == isLeapYear(date.getYear())));
        date = date.minusMonths(100);
        System.out.println("countDayOfYear: " + (date.getDayOfYear() == countDayOfYear(date.getDayOfMonth(), date.getMonthValue(), date.getYear())));
        System.out.println("isLeapYear: " + (date.isLeapYear() == isLeapYear(date.getYear())));
        date = date.minusYears(100);
        System.out.println("countDayOfYear: " + (date.getDayOfYear() == countDayOfYear(date.getDayOfMonth(), date.getMonthValue(), date.getYear())));
        System.out.println("isLeapYear: " + (date.isLeapYear() == isLeapYear(date.getYear())));

        date = LocalDate.of(2020, 3, 17);
        System.out.println("countDayOfYear: " + (date.getDayOfYear() == countDayOfYear(date.getDayOfMonth(), date.getMonthValue(), date.getYear())));

        for (int i = 0; i < 100; i++) {
            date = LocalDate.now().minusDays(new Random().nextInt(2023 * 365));
            System.out.print("date: " + date);
            long duration = Duration.between(date.atStartOfDay(), LocalDate.now().atStartOfDay()).toDays();
            int d2 = solution(String.format("%s-%s-%s", date.getDayOfMonth(), date.getMonthValue(), date.getYear()));
            System.out.println(" - res: " + (duration == d2) + ": " + duration + " vs " + d2);
        }

    }
}
