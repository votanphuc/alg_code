package code032023;

import java.util.Arrays;
import java.util.stream.Collectors;

public class MakeAbbreviation {
    static StringBuilder solution(String input) {
        StringBuilder res = new StringBuilder();
        for (int i = 1; i < input.length(); i++) {
            if ('A' <= input.charAt(i) && 'Z' >= input.charAt(i)) {
                res.append(input.charAt(i));
            }
        }
        return res;
    }

    static String solution2(String input) {
        StringBuilder res = new StringBuilder();
        int i = 0;
        while (input.charAt(i) == ' ') {
            i++;
        }

        if ('A' <= input.charAt(i) && 'Z' >= input.charAt(i)) {
            res.append(input.charAt(i));
            i++;
        }

        for (; i < input.length() - 1; i++) {
            if (input.charAt(i) == ' ' && 'A' <= input.charAt(i + 1) && 'Z' >= input.charAt(i + 1)) {
                res.append(input.charAt(i + 1));
            }
        }

        return res.toString();
    }


    public static void main(String[] args) {
        String str = "  HapPy New new  _ Year  E +noYear";
        String result = Arrays.stream(str.split(" "))
                .map(s -> s.length() > 0 && 'A' <= s.charAt(0) && 'Z' >= s.charAt(0) ? s.charAt(0) : null)
                .collect(StringBuilder::new, (stringBuilder, codePoint) -> {
                    if (codePoint != null)
                        stringBuilder.appendCodePoint(codePoint);
                }, StringBuilder::append)
                .toString();

        System.out.println(result);

        System.out.println(solution2(str));
    }
}
