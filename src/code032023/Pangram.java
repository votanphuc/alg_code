package code032023;

import java.util.HashSet;
import java.util.Set;

public class Pangram {

    public static String pangrams(String s) {
        // Write your code here
        Set<Character> schars = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            if (' ' == s.charAt(i)) {
                continue;
            }

            schars.add(Character.toLowerCase(s.charAt(i)));

            if (schars.size() == 26) {
                System.out.println(schars);
                return "pangram";
            }
        }
        return "not pangram";

    }

    public static void main(String[] args) {
        System.out.println( pangrams("We promptly judged antique ivory buckles for the prize"));
    }
}
