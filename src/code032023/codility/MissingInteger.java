package code032023.codility;

import java.util.HashSet;

/**
 * This is a demo task.
 * Write a function:
 * class Solution { public int solution(int[] A); }
 * that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
 * For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
 * Given A = [1, 2, 3], the function should return 4.
 * Given A = [−1, −3], the function should return 1.
 * Write an efficient algorithm for the following assumptions:
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [−1,000,000..1,000,000].
 */
public class MissingInteger {

    public int solution(int[] A) {
        int smallest = 1;
        HashSet<Integer> set = new HashSet<>();
        for (int v : A) {
            if (v <= 0) continue;
            set.add(v);
            while (set.contains(smallest)) {
                set.remove(smallest);
                smallest++;
            }
        }
        return smallest;
    }

    public static void main(String[] args) {
        MissingInteger test = new MissingInteger();
        int[] largePermutation = new int[999999];
        for (int i = 0; i < 999999; i++) {
            largePermutation[i] = i + 1;
        }
//        largePermutation = new int[]{2, 3, 1, 4, 6, 5, 8, 9, 4};
        System.out.println(test.solution(largePermutation));
    }
}
