package code032023.codility;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * An array A consisting of N different integers is given.
 * The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.
 * Your goal is to find that missing element.
 * Write a function:
 * class Solution { public int solution(int[] A); }
 * that, given an array A, returns the value of the missing element.
 * For example, given array A such that:
 * A[0] = 2
 * A[1] = 3
 * A[2] = 1
 * A[3] = 5
 * the function should return 4, as it is the missing element.
 * Write an efficient algorithm for the following assumptions:
 * N is an integer within the range [0..100,000];
 * the elements of A are all distinct;
 * each element of array A is an integer within the range [1..(N + 1)].
 */
public class PermMissingElem {

    public int solution(int[] A) {
        BigDecimal n = BigDecimal.valueOf(A.length + 1);
        BigDecimal sum = n.multiply(BigDecimal.valueOf(A.length + 2)).divide(BigDecimal.valueOf(2), RoundingMode.UP); // n*(n+1)/2
        BigDecimal sum_permutation = BigDecimal.ZERO;
        for (int v : A) {
            sum_permutation = sum_permutation.add(BigDecimal.valueOf(v));
        }
        return sum.subtract(sum_permutation).intValue();
    }

    public int solution2(int[] A) {
        Set<Integer> set = new HashSet<>();
        for (int i : A) {
            set.add(i);
        }
        int n = A.length + 1;
        for (int i = 1; i <= n; i++) {
            if (!set.contains(i)) {
                return i;
            }
        }
        return -1; // missing element not found
    }

    public static void main(String[] args) {
        PermMissingElem test = new PermMissingElem();
        int[] largePermutation = new int[999999];
        for (int i = 0; i < 999999; i++) {
            largePermutation[i] = i + 1;
        }
//        largePermutation = new int[]{2, 3, 1, 5, 6};
        System.out.println(test.solution(largePermutation));
    }
}
