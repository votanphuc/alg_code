package code032023.codility;

/**
 * Write a function:
 * class Solution { public int solution(int A, int B, int K); }
 * that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:
 * { i : A ≤ i ≤ B, i mod K = 0 }
 * For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.
 * Write an efficient algorithm for the following assumptions:
 * A and B are integers within the range [0..2,000,000,000];
 * K is an integer within the range [1..2,000,000,000];
 * A ≤ B.
 */
public class CountDiv {

    public int solution2(int A, int B, int K) { // O(1)
        int i = A;

        while (i % K != 0) {
            i++;
            if (i > B) {
                return 0;
            }
        }
        return (B - i) / K + 1;
    }

    public static void main(String[] args) {
        CountDiv test = new CountDiv();
        System.out.println(test.solution2(0, 0, 11));

    }
}
