package code032023;

import java.util.HashSet;
import java.util.Set;

public class CheckMultiplyDigit {

    static boolean solution(long number) {
        Set<Long> digits = new HashSet<>();
        long mNumber = number * 2;

        if (mNumber > 999999) {
            return false;
        }

        while (number > 0) {
            digits.add(number % 10);
            number /= 10;
        }

        while (mNumber > 0) {
            if (!digits.contains(mNumber % 10)) {
                return false;
            }
            mNumber /= 10;
        }

        return true;
    }


    public static void main(String[] args) {
        System.out.println(solution(142857));
    }
}
