package code032023;

import java.util.Map;
import java.util.Stack;

public class CountBracket {
    static Map<Character, Character> map = Map.of('{', '}', '[', ']');

    static boolean solution(String bracket) {

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < bracket.length(); i++) {
            if (map.containsKey(bracket.charAt(i))) {
                stack.push(bracket.charAt(i));
            } else if (map.containsValue(bracket.charAt(i))) {
                if (stack.isEmpty() || map.get(stack.pop()) != bracket.charAt(i)) {
                    return false;
                }
            }
        }

        return stack.isEmpty();
    }

    static boolean recursive(String bracket) {
        return recursive(bracket, 0, new Stack<>());
    }

    static boolean recursive(String bracket, int index, Stack<Character> stack) {
        if (bracket.length() == index) {
            return stack.isEmpty();
        }

        if (map.containsKey(bracket.charAt(index))) {
            stack.push(bracket.charAt(index));
        } else if (map.containsValue(bracket.charAt(index))) {
            if (stack.isEmpty() || map.get(stack.pop()) != bracket.charAt(index)) {
                return false;
            }
        }

        return recursive(bracket, index + 1, stack);
    }

    static boolean recursive2(String bracket, Stack<Character> stack) {
        if (bracket.isEmpty()) {
            return stack.isEmpty();
        }

        if (map.containsKey(bracket.charAt(0))) {
            stack.push(bracket.charAt(0));
        } else if (map.containsValue(bracket.charAt(0))) {
            if (stack.isEmpty() || map.get(stack.pop()) != bracket.charAt(0)) {
                return false;
            }
        }

        return recursive2(bracket.substring(1), stack);
    }


    public static void main(String[] args) {
        System.out.println(recursive("{[]{}{{}}[[]]{{{[[[]]]}}}}"));
    }
}
