package code032023;

import java.util.ArrayList;

public class ArraySet {

    private boolean findTotal2Loop(ArrayList<Integer> list, int n) {
        for (int i = 0; i < list.size() - 1; i++) {
            System.out.println("n=" + n + " v=" + list.get(i));
            n -= list.get(i);
            for (int j = i + 1; j < list.size(); j++) {
                if (n - list.get(j) == 0) {
                    return true;
                }

                if (n - list.get(j) < 0) {
                    break;
                }
            }
        }
        return false;
    }

    public boolean checkTotal(int[] list, int n) {
        for (int i = 0; i < list.length - 1; i++) {
            int sum = list[i];
            if (sum > n) continue;
            for (int j = i + 1; j < list.length; j++) {
                sum += list[j];
                if (sum == n) return true;
                if (sum > n) sum -= list[j];
            }
        }

        return false;
    }

    public boolean checkTotalThenPrint(int[] list, int n) {
        for (int i = 0; i < list.length - 1; i++) {
            if (list[i] >= n) continue; // double check case 1 number is allow or not

            int sum = list[i];
            ArrayList<Integer> nums = new ArrayList<>();
            nums.add(list[i]);

            for (int j = i + 1; j < list.length; j++) {
                if (sum + list[j] > n) {
                    continue;
                }

                nums.add(list[j]);
                sum += list[j];
                if (sum == n) {
                    System.out.println(nums);
                    return true;
                }
            }
        }

        return false;
    }

    public static void main(String[] args) {
        ArraySet demo = new ArraySet();
        for (int i = 0; i < 50; i++) {
            boolean pair = demo.checkTotalThenPrint(new int[]{10, 20, 2, 9, 10, 3, 11, 15, 18, 6, 13}, i);
            System.out.println(i + " - " + pair);
            System.out.println();
        }
    }
}
