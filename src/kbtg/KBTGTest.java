package kbtg;

public class KBTGTest {

    /**
     * ..<.<>>..>. = 2+3+3+1
     */
    int countDot(String str) {
        int countLeft = 0;
        int tempCountLeft = 0;
        int countRight = 0;
        int tempCountRight = 0;
        for (int i = 0; i < str.length(); i++) {
            char charLeft = str.charAt(i);
            if (charLeft == '.') {
                tempCountLeft++;
            } else if (str.charAt(i) == '<') {
                countLeft += tempCountLeft;
            }

            char charRight = str.charAt(str.length() - i - 1);
            if (charRight == '.') {
                tempCountRight++;
            } else if (charRight == '>') {
                countRight += tempCountRight;
            }
        }
        return countLeft + countRight;
    }

    /**
     * 20.13 - 20.45 -> 20.15 - 20.45 -> 2
     * 20.15 - 20.46 -> 20.15 - 20.45 -> 2
     * 20.00 -> 01:00 -> 5 * 4 -> 20
     */
    int countRange15(String timeIn, String timeOut) {
        String[] splitIn = timeIn.split(":");
        int hourIn = Integer.parseInt(splitIn[0]);
        int minIn = Integer.parseInt(splitIn[1]);

        int roundedMinIn = minIn + 14 - (minIn + 14) % 15;
        int totalMinIn = hourIn * 60 + roundedMinIn;

        String[] splitOut = timeOut.split(":");
        int hourOut = Integer.parseInt(splitOut[0]);
        int minOut = Integer.parseInt(splitOut[1]);

        if (hourOut < hourIn) {
            hourOut += 24;
        }
        int roundedMinOut = minOut - minOut % 15;
        int totalMinOut = hourOut * 60 + roundedMinOut;

        return (totalMinOut - totalMinIn) / 15;
    }

    int findMax(int number, int numberWBR) {
        String numStr = Integer.toString(number);
        if (!numStr.contains(Integer.toString(numberWBR))) {
            return number;
        }

        int maxNumber = Integer.MIN_VALUE;
        for (int i = 0; i < numStr.length(); i++) {
            if (Character.getNumericValue(numStr.charAt(i)) == numberWBR) {
                String halfLeft = numStr.substring(0, i);
                String halfRight = numStr.substring(i + 1);
                int removedNumber = Integer.parseInt(halfLeft + halfRight);
                maxNumber = Math.max(removedNumber, maxNumber);
            }
        }
        return maxNumber;
    }

    public static void main(String[] args) {
        KBTGTest t = new KBTGTest();
//        System.out.println(t.countDot("..<.<>..>.>."));//2+3+4+2+1
//        System.out.println(t.countRange15("20:13", "20:45"));
        System.out.println(t.findMax(123142, 4));
    }
}
