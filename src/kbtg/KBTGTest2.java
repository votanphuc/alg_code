package kbtg;

public class KBTGTest2 {


    /**
     * 20.13 - 20.45 -> 20.15 - 20.45 -> 2
     * 20.15 - 20.46 -> 20.15 - 20.45 -> 2
     * 20.00 -> 01:00 -> 5 * 4 -> 20
     */
    int countRange15(String timeIn, String timeOut) {
        String[] splitIn = timeIn.split(":");
        int hourIn = Integer.parseInt(splitIn[0]);
        int minIn = Integer.parseInt(splitIn[1]);

        int roundedMinIn = minIn + 14 - (minIn + 14) % 15;
        int totalMinIn = hourIn * 60 + roundedMinIn;

        String[] splitOut = timeOut.split(":");
        int hourOut = Integer.parseInt(splitOut[0]);
        int minOut = Integer.parseInt(splitOut[1]);

        if (hourOut < hourIn) {
            hourOut += 24;
        }
        int roundedMinOut = minOut - minOut % 15;
        int totalMinOut = hourOut * 60 + roundedMinOut;

        return Math.max((totalMinOut - totalMinIn) / 15, 0);
    }

    int sol(int N) {
        // Implement your solution here
        int max = Integer.MIN_VALUE;
        String num = Integer.toString(N);

        for (int i = 0; i < num.length(); i++) {
            if (5 == Character.getNumericValue(num.charAt(i))) {
                int v = Integer.parseInt(num.substring(0, i) + num.substring(i + 1));
                max = Math.max(max, v);
            }
        }

        return max;
    }

    public static void main(String[] args) {
        KBTGTest2 sol = new KBTGTest2();
//        System.out.println(sol.countRange15("22:45", "01:55"));
        System.out.println(sol.sol(15958));
    }

}
