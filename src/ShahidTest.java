import java.util.Map;
import java.util.Stack;

public class ShahidTest {

    public static int solution(int input) { // 123
        int count = 0;

        while (input > 0) {
            count += input % 10;
            input /= 10;
        }

        return count;
    }

    /**
     * {{}} = valid
     * {(<>}) = invalid
     */
    public boolean checkBracket(String input) {
        Map<Character, Character> map = Map.of('{', '}' , '(', ')', '<', '>');
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i <input.length(); i++) {
            if (map.containsKey(input.charAt(i))) {
                stack.push(input.charAt(i));
                continue;
            }

            if (map.containsValue(input.charAt(i))) {
                if (map.get(stack.pop()) != input.charAt(i)){
                    return false;
                }
            }
        }

        return true;
    }

    // maddam = ture
    // dad = true
    // botato = false
    public static boolean checkSame(String input) {
        int i = 0;
        while (i < input.length()/2) {
            int last = input.length() - 1 - i;
            if (input.charAt(i) != input.charAt(last)) {
                return false;
            }

            i++;
        }

        return true;
    }


    public static void main(String[] args) {
//        System.out.println(solution(123));
//        System.out.println(checkSame("aaa"));

//        ExSingleton mySingleton = ExSingleton.getInstance();
    }
}
