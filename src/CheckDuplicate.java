
/*
 * Cho trước 1 string, tìm một ký tự trong string đó mà xuất hiện nhiều hơn 2 lần.
 * Có thể optimize giải thuật mà không dùng thêm bộ nhớ và CTDL phụ như map/set không?
 */
public class CheckDuplicate {

    public static boolean duplicateChar(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (str.substring(i + 1).contains(String.valueOf(str.charAt(i)))) {
                return true;
            }
        }
        return false;
    }

    public static boolean duplicateInt(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        String str = "votanphuc";
        System.out.println(duplicateChar(str));
        int[] arr = {1, 2, 3, 4, 5};
        System.out.println(duplicateInt(arr));
    }
}
