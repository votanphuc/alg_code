import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;

//public class Compares {
//    public static void main(String args[]) {
//        String[] cities = {"Bangalore", "Pune", "San Francisco", "New York City"};
////        MySort ms = new MySort();
//        Arrays.sort(cities, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//               return o2.compareTo(o1);
//            }
//        });
//        System.out.println(Arrays.toString(cities));
//        System.out.println(Arrays.binarySearch(cities, "New York City"));
//    }
//
//}

public class Compares {
    public static void main(String args[]) {
        String[] cities = {"Bangalore", "Pune", "San Francisco", "New York City"};
        MySort ms = new MySort();
        Arrays.sort(cities, ms);
        System.out.println(Arrays.binarySearch(cities, "New York City"));
    }

    static class MySort implements Comparator<String> {
        public int compare(String a, String b) {
            return b.compareTo(a);
        }
    }
}

class Limits {
    private int x = 2;
    protected int y = 3;
    private static int x1 = 4;
    protected static int x2 = 5;

    public static void main(String[] args) {
        int x = 6;
        int y = 7;
        int x1 = 8;
        int x2 = 9;
        new Limits().new Secret().go();
    }

    class Secret {
        void go() {
            System.out.println(x + " " + y + " " + x1 + " " + x2);
        }
    }
}

class MyThread extends Thread {
    MyThread() {
        System.out.print(" MyThread");
    }

    public void run() {
        System.out.print(" bar");
    }

    public void run(String s) {
        System.out.print(" baz");
    }
}

class TestThreads {
    public static void main(String[] args) {
        Thread t = new MyThread() {
            public void run() {
                System.out.print(" foo");
            }
        };
        t.start();
    }
}