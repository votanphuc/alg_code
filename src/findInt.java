import java.util.HashSet;
import java.util.List;

public class findInt {


    public static void solution(List<Integer> l, int t) {
        boolean flag = false;
        int min = 0;
        HashSet<Integer> hashSet = new HashSet<>();
        for (int i = 0; i < l.size(); i++) {
            if (hashSet.contains(t - l.get(i))) {
                if (!flag) {
                    min = Math.min(l.get(i), t - l.get(i));
                } else {
                    min = Math.min(min, Math.min(l.get(i), t - l.get(i)));
                }
                flag = true;
            }

            hashSet.add(l.get(i));
        }
        if (flag) {
            System.out.println(Math.min(min, t - min) + " " + Math.max(min, t - min));
        } else {
            System.out.println(-1);
        }
    }


    public static void solution2() {
        int a = 15;
        int h = 10;
        double w = Math.sqrt(a * a - h * h);
        double r = (w * w / 8 * h) + (h / 2);
        System.out.println(r);
    }

    public static void main(String[] args) throws java.lang.Exception {
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        String input = br.readLine();
//
//        List<Integer> l = new ArrayList<>();
//        for (String s : input.split(" ")) {
//            l.add(Integer.parseInt(s));
//        }
//
//        System.out.println(l);
//        int t = Integer.parseInt(br.readLine());
//        solution(l, t);

        solution2();
    }
}
