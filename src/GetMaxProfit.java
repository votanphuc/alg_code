import java.util.ArrayList;
import java.util.List;

public class GetMaxProfit {

    static void maxDiff(int[] a) {
        int first = 0;
        int last = a.length - 1;
        int gap = a[first] - a[last];
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (gap < a[j] - a[i]) {
                    gap = a[j] - a[i];
                    first = i;
                    last = j;
                }
            }
        }
        System.out.println(first + " - " + last + " - " + gap);
    }

    static int maxDiff2(int[] a) {
        int gap = a[1] - a[0];
        int min = a[0];
        for (int i = 1; i < a.length; i++) {
            gap = Math.max(a[i] - min, gap);
            min = Math.min(a[i], min);
        }
        return gap;
    }

    static int maxDiff(int arr[], int arr_size) {
        int min_index = 0;
        int max_index = 1;
        int max_diff = arr[max_index] - arr[min_index];
        for (int i = 1; i < arr_size; i++) {
            if (arr[i] - arr[min_index] > max_diff) {
                max_diff = arr[i] - arr[min_index];
                max_index = i;
            }
            if (arr[i] < arr[min_index]) {
                min_index = i;
            }
        }
        System.out.println(max_index + "-" + min_index);
        return max_diff;
    }


    static void stockBuySell(int[] a) {
        if (a.length <= 1) {
            return;
        }

        List<Interval> intervals = new ArrayList<>();

        int i = 0;
        while (i < a.length - 1) {
            while (i < a.length - 1 && a[i + 1] <= a[i]) {
                i++;
            }

            if (i == a.length - 1) {
                break;
            }

            Interval itv = new Interval();
            itv.buy = i++;

            while (i < a.length && a[i] >= a[i - 1]) {
                i++;
            }

            itv.sell = i - 1;
            intervals.add(itv);
        }

        for (Interval interval : intervals) {
            System.out.print(interval.buy);
            System.out.println(interval.sell);
        }
    }

    static void stockBuySell(int price[], int n) {
        if (n == 1)
            return;

        int count = 0;

        ArrayList<Interval> sol = new ArrayList<Interval>();

        int i = 0;
        while (i < n - 1) {
            while ((i < n - 1) && (price[i + 1] <= price[i]))
                i++;

            if (i == n - 1)
                break;

            Interval e = new Interval();
            e.buy = i++;

            while ((i < n) && (price[i] >= price[i - 1]))
                i++;

            e.sell = i - 1;
            sol.add(e);

            count++;
        }

        if (count == 0)
            System.out.println("There is no day when buying the stock "
                    + "will make profit");
        else
            for (int j = 0; j < count; j++)
                System.out.println("Buy on day: " + sol.get(j).buy
                        + "        "
                        + "Sell on day : " + sol.get(j).sell);

        return;
    }

    public static void main(String[] args) {
        int a[] = {4, 7, 3, 5, 8};
        System.out.println(maxDiff(a, a.length));
//        stockBuySell(a);
//        stockBuySell(a, a.length);
    }

}

class Interval {
    int buy, sell;
}
