public class SomeAlgCodility {

    public int solutionFindInteger(int[] A) {
        int max = A.length;
        int res = 1;
        boolean[] bitmap = new boolean[max + 1];

        //populate bitmap and also find highest positive int in input list.
        for (int i = 0; i < A.length; i++) {
            if (A[i] > 0 && A[i] <= max) {
                bitmap[A[i]] = true;
            }

            if (A[i] > res) {
                res = A[i];
            }
        }

        for (boolean b : bitmap) {
            System.out.println(b);
        }

        //find the first positive number in bitmap that is false.
        for (int i = 1; i < bitmap.length; i++) {
            if (!bitmap[i]) {
                return i;
            }
        }
        //this is to handle the case when input array is not missing any element.
        return (res + 1);
    }

    public int solutionBetAllin(int N, int K) {
        int time = 0;
        if (Math.pow(2, K) > N) {
            System.out.println("aaa");
        }
        if (K == 0) {
            return N - 1;
        }

        while (N > 1) {
            System.out.print("N: " + N);
            if (N % 2 == 0 && K > 0) {
                N /= 2;
                K--;
                System.out.print("\tK:" + K);
            } else {
                N--;
            }
            time++;
            System.out.println();
        }
        return time;
    }

    private boolean isContain(int[] A, int K, int start, int end) {
        for (int i = start; i < end; i++) {
            if (A[i] == K) {
                System.out.println(true);
                return true;
            }
        }
        return false;
    }

    public int solutionGetShortArray(int[] A) { //incomplete
        int result = A.length;

        int i = 0;
        int j = A.length - 1;

        while (i < A.length - 1) {
            System.out.println("Num: " + A[i] + " range: " + i + " - " + j);
            if (isContain(A, A[i], i + 1, j)) {
                result = j - i;
                i++;
            } else {
                break;
            }
        }

        while (j > 0) {
            System.out.println("Num: " + A[j] + " range: " + i + " - " + j);
            if (isContain(A, A[j], i, j - 1)) {
                result = j - i;
                j--;
            } else {
                break;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] A = {7, 3, 1, 3, 4, 7, 3, 1, 1, 3};
        SomeAlgCodility t = new SomeAlgCodility();
//        System.out.println("Res: " + t.solutionBetAllin(9, 4));
//        System.out.println("Res: " + t.solutionBetAllin(7, 3));
//        System.out.println("Res: " + t.solutionBetAllin(11, 3));
//        System.out.println("Res: " + t.solutionBetAllin(11, 10));

        System.out.println(t.solutionGetShortArray(A));
    }
}
