package hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'separateNumbers' function below.
     *
     * The function accepts STRING s as parameter.
     */

    public static List<Integer> icecreamParlor(int m, List<Integer> arr) {
        // Write your code here
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.size(); i++) {
            int value = m - arr.get(i);
            if (!map.containsValue(value)) {
                map.put(i, arr.get(i));
            } else {
                return List.of(map.entrySet().stream().filter(e -> e.getValue() == value).map(Map.Entry::getKey).findFirst().get() + 1, i + 1);
            }
        }
        return Collections.emptyList();
    }

    public static <T, E> Optional<T> getKeysByValue(Map<T, E> map, E value) {
        return map.entrySet()
                .stream()
                .filter(entry -> Objects.equals(entry.getValue(), value))
                .map(Map.Entry::getKey)
                .findFirst();
    }

    public static void separateNumbers(String s) {
        // Write your code here
        int digitNo = 1;

        while (digitNo <= s.length() / 2) {
            long f1 = Integer.parseInt(s.substring(0, digitNo));
            long f2 = f1 + 1;

            int i = digitNo;
            while (true) {
                int size = countNum(f2);
                if (i + size > s.length()) {
                    break;
                }
                if (f2 != Integer.parseInt(substring(s, i, size))) {
                    break;
                }
                if (i + size == s.length()) {
                    System.out.println("YES " + f1);
                    return;
                }
                f2++;
                i += size;
            }

            digitNo++;
        }

        System.out.println("NO");
    }

    private static String substring(String s, int start, int size) {
        return s.substring(start, start + size);
    }

    private static int countNum(long n) {
        int count = 0;
        while (n > 0) {
            count++;
            n /= 10;
        }

        return count;
    }

    public static int pickingNumbers(List<Integer> a) {
        // Write your code here
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int n : a) {
            map.compute(n, (k, v) -> {
                if (v == null) {
                    v = 1;
                } else {
                    v++;
                }
                return v;
            });

        }

        int res = 0;
        for (int n : map.keySet()) {
            res = map.get(n) > res ? map.get(n) : res;
            if (map.get(n + 1) == null) {
                continue;
            }

            int count = map.get(n) + map.get(n + 1);
            if (count > res) {
                res = count;
            }
        }
        return res;

    }

    public static void countApplesAndOranges(int s, int t, int a, int b, List<Integer> apples, List<Integer> oranges) {
        // Write your code here
        // a + x > s
        // b + y < t

        int aCount = 0;
        for (int p : apples) {
            if (a + p > s) {
                aCount++;
            }
        }
        System.out.println(aCount);

        int oCount = 0;
        for (int o : oranges) {
            if (b + o < t) {
                oCount++;
            }
        }
        System.out.println(oCount);
    }

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int s = Integer.parseInt(firstMultipleInput[0]);

        int t = Integer.parseInt(firstMultipleInput[1]);

        String[] secondMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int a = Integer.parseInt(secondMultipleInput[0]);

        int b = Integer.parseInt(secondMultipleInput[1]);

        String[] thirdMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int m = Integer.parseInt(thirdMultipleInput[0]);

        int n = Integer.parseInt(thirdMultipleInput[1]);

        List<Integer> apples = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        List<Integer> oranges = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        Result.countApplesAndOranges(s, t, a, b, apples, oranges);

        bufferedReader.close();
    }
}
