
/*
* Description
The rectangle is a figure with four straight sides and four right angles. Given 3 points of a rectangle in a coordinate plane, figure out the last point to make a rectangle.

v is an array, containing 3 points of a rectangle. By using v, make a solution function that returns the last point.

Constraints
v is an array, containing 3 points of a triangle.
Each of v's element has x and y coordinates, which represents a point of a rectangle.
In v, two points can be connected either with a vertical or horizontal line.
Each coordinate is a natural number, less than or equal to 10 billion.
Return the last point, [coordiante x, coordinate y].
Examples
v	result
[[1, 4], [3, 4], [3, 10]]	[1, 10]
[[1, 1], [2, 2], [1, 2]]	[2, 1]
Example #1
When there are 3 points: [1, 4], [3, 4], [3, 10]
[1,10] will make a rectangle.

Example #2
When there are 3 points: [[1, 1], [2, 2], [1, 2]]
[2,1] will make a rectangle.
*
*
* ==================================*/

/*
* Description
※ Use Standrad input and output to solve this challenge

Print a n by m grid of asterisks.

Constrratins
The first line contains 2-separated integers, n and m.

1 ≤ n, m ≤ 1,000
Examples
Input

5 3
Output

*****
*****
******/


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class LINETest {
    public static int[] solution2(int[][] v) {
        int x = 0;
        int y = 0;


        int[] answer = {x, y};
        return answer;
    }

    public static String solution() {
        List<Integer> s = new ArrayList<>();
        return "";
    }


    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        int a = sc.nextInt();
//        int b = sc.nextInt();
//
//        for (int i = 0; i < a; i++) {
//            for (int j = 0; j < b; j++) {
//                System.out.println('*');
//            }
//        }

//        String s = "RemoteIo is awesome";
//        for (String str : s.split(" ")) {
//            for (int i = str.length() - 1; i >= 0; i--) {
//                System.out.print(str.charAt(i));
//            }
//            System.out.print(" ");
//        }
//       oIetomeR si emosewa

        ArrayList<Integer> s = new ArrayList<>();

        float a = 10f;
        double v = Math.round(a * 10000.0) / 10000.0;
        System.out.println(v);

    }

    public static void main2(String[] args) throws java.lang.Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = br.readLine();
        int n = Integer.parseInt(input.split(" ")[0]);
        int c = Integer.parseInt(input.split(" ")[1]);

        ArrayList<String> q = new ArrayList<>();
        int i = 0;

        String str = "";
        while ((str = br.readLine()) != null) {
            switch (str.charAt(0)) {
                case 'O':
                    if (c <= 0) {
                        System.out.println("false");
                        break;
                    }
                    q.add(str.replace("OFFER ", ""));
                    c--;
                    System.out.println("true");
                    break;

                case 'T':
                    System.out.println(q.get(i));
                    i++;
                    c++;
                    break;

                case 'S':
                    System.out.println(c - i);
                    break;
            }
        }
    }
}


/*

import java.io.*;
import java.util.*;

class myCode
{
    public static void solution(int w, List<Integer> s) {
        int j, max;

        for (int i = 0; i <= s.size() - w; i++) {
            max = s.get(i);
            for (j = 1; j < w; j++) {
                if (s.get(i + j) > max)
                    max = s.get(i + j);
            }
            System.out.println(max + " ");
        }
    }

    public static void main (String[] args) throws java.lang.Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int w = Integer.parseInt(br.readLine());

        List<Integer> s = new ArrayList<>();
        String str = "";
        while((str = br.readLine()) != null){
            s.add(Integer.parseInt(str));
        }

        solution(w, s);
    }
}
 */