package thai_test;

import java.util.HashSet;
import java.util.Set;

public class CheckTimeDigit {

    public int solution(int A, int B, int C, int D) {

        Set set = new HashSet<>();

        int[] timeDigit = {A, B, C, D};
        for (int i = 0; i < timeDigit.length; i++) {
            for (int j = 0; j < timeDigit.length; j++) {
                if (j != i) {
                    for (int k = 0; k < timeDigit.length; k++) {
                        if (k != i && k != j) {
                            for (int l = 0; l < timeDigit.length; l++) {
                                if (l != i && l != j && l != k) {
                                    int hour = Integer.parseInt("" + timeDigit[i] + timeDigit[j]);
                                    int minute = Integer.parseInt("" + timeDigit[k] + timeDigit[l]);
                                    if (hour < 24 && minute < 60) {
                                        set.add(String.format("%d:%d", hour, minute));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println(set.toString());
        return set.size();
    }

    public static void main(String[] args) {
        CheckTimeDigit checkTime = new CheckTimeDigit();

        System.out.println(checkTime.solution(2,3,3,2));

    }
}
