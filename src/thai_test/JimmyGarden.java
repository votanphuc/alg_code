package thai_test;

public class JimmyGarden {

    public static void main(String[] args) {
        int[] A = new int[]{3, 4, 5, 3, 7};
        System.out.println("Result: " + new JimmyGarden().solution(A));
        A = new int[]{1, 2, 3, 4};
        System.out.println("Result: " + new JimmyGarden().solution(A));
        A = new int[]{1, 3, 1, 2};
        System.out.println("Result: " + new JimmyGarden().solution(A));
        A = new int[]{1, 1, 2};
        System.out.println("Result: " + new JimmyGarden().solution(A));
    }

    public int solution(int[] A) {
        if (isPleasingTrees(A)) {
            return 0;
        }

        int count = 0;
        for (int i = 0; i < A.length; i++) {
            int[] B = removeOneTree(A, i);
            if (isPleasingTrees(B)) {
                count++;
            }
        }

        if (count == 0) {
            return -1;
        } else {
            return count;
        }
    }

    public boolean isPleasingTrees(int[] B) {
        boolean up = false;
        if (B[0] == B[1]) {
            return false;
        } else if (B[0] < B[1]) {
            up = true;
        } else {
            up = false;
        }

        for (int j = 1; j < B.length - 1; j++) {
            if (up) {
                if (B[j] <= B[j + 1]) {
                    return false;
                }
            } else {
                if (B[j] >= B[j + 1]) {
                    return false;
                }
            }
            up = !up;
        }
        return true;
    }

    public int[] removeOneTree(int[] A, int i) {
        int[] B = new int[A.length - 1];
        int k = 0;
        for (int j = 0; j < A.length; j++) {
            if (j != i) {
                B[k] = A[j];
                k++;
            }
        }
        return B;
    }
}
