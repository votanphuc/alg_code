package thai_test;

public class Circles {

    public int solution(int inner, int outer, int[] points_x, int[] points_y) {
        int count = 0;
        for (int i = 0; i < points_x.length; i++) {
            if (inner < getDistance(points_x[i], points_y[i]) && getDistance(points_x[i], points_y[i]) < outer) {
                count++;
            }
        }
        return count;
    }

    public double getDistance(int x, int y) {
        return Math.sqrt(x * x + y * y);
    }

    public static void main(String[] args) {
        int inner = 1, outer = 5;
        int[] point_x = new int[]{4, 0, 1, -2};
        int[] point_y = new int[]{-4, 4, 3, 0};

        System.out.println("Result: " + (new Circles()).solution(inner, outer, point_x, point_y));
    }
}
