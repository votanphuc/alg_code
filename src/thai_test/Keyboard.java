package thai_test;

import java.util.Stack;

public class Keyboard {

    public String solution(String S) {
        boolean isOnCapsLock = false;

        Stack letters = new Stack();
        for (int i = 0; i < S.length(); i++) {
            if (S.charAt(i) == 'C') {
                isOnCapsLock = !isOnCapsLock;
            } else if (S.charAt(i) == 'B') {
                if (!letters.empty()) {
                    letters.pop();
                }
            } else {
                if (isOnCapsLock) {
                    letters.push(Character.toUpperCase(S.charAt(i)));
                } else {
                    letters.push(Character.toLowerCase(S.charAt(i)));
                }
            }
            System.out.println(letters);
        }

        return letters.toString().replace("[", "").replace(", ", "").replace("]", "");
    }

    public static void main(String[] args) {
        System.out.println((new Keyboard()).solution("aCaBBCCab"));
    }
}
