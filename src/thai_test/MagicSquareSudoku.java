package thai_test;

public class MagicSquareSudoku {

    public static void main(String[] args) {
//        int[][] A = new int[][]{
//            {4,3,4,5,3},
//            {2,7,3,8,4},
//            {1,7,6,5,2},
//            {8,4,9,5,5}
//        };
//        System.out.println("Result: " + solution(A));
//        int[][] A = new int[][]{
//            {2,2,1,1},
//            {2,2,2,2},
//            {1,2,2,2}
//        };
//        System.out.println("Result: " + solution(A));
        int[][] A = {
                {7, 2, 4},
                {2, 7, 6},
                {9, 5, 1},
                {4, 3, 8},
                {3, 5, 4}
        };
        System.out.println("Result: " + solution(A));
    }

    //A là ma trận N x M (có thể không vuông)
    public static int solution(int[][] A) {
        int N = A.length;//Lấy ra số dòng của A
        int M = A[0].length;//Lấy ra số cột của A
        int K = Math.min(N, M);//K là kích thước lớn nhất của magic
        for (int k = K; k > 1; k--) {
            for (int i = 0; i <= N - k; i++) {
                for (int j = 0; j <= M - k; j++) {
                    int[][] sm = getSubMatrix(A, k, i, j);
                    if (isMagic(sm)) {
                        //For test
                        display(sm);
                        //End for test
                        return k;
                    }
                }
            }
        }
        return 1;//one
    }

    //Hiển thị ma trận vuông A
    public static void display(int[][] A) {
        for (int[] row : A) {
            for (int j = 0; j < row.length; j++) {
                System.out.print(row[j] + " ");
            }
            System.out.println();
        }
    }

    //Lấy ra ma trận con K x K trong ma trận A tại vị trí (i,j)
    public static int[][] getSubMatrix(int[][] A, int K, int i, int j) {
        int[][] sm = new int[K][K];//sm = sub matrix
        for (int k = 0; k < K; k++) {
            int r = i + k;
            for (int l = 0; l < K; l++) {
                int c = j + l;
                //System.out.printf("%d,%d,%d,%d,%d\n",K,k,l,r,c);//For test
                sm[k][l] = A[r][c];
            }
        }
        return sm;
    }

    //Kiểm tra ma trận vuông A là magic
    public static boolean isMagic(int[][] A) {
        int sum_of_row0 = sumOfRow(A, 0);

        for (int i = 1; i < A.length; i++) {
            if (sumOfRow(A, i) != sum_of_row0) {
                return false;
            }
        }

        for (int i = 0; i < A.length; i++) {
            if (sumOfColumn(A, i) != sum_of_row0) {
                return false;
            }
        }
        if (sumOfPrimaryDiagonal(A) != sum_of_row0) {
            return false;
        }

        if (sumOfSecondaryDiagonal(A) != sum_of_row0) {
            return false;
        }

        return true;
    }

    //Tính tổng các phần tử trên dòng i của ma trận vuông A
    public static int sumOfRow(int[][] A, int i) {
        int sum = 0;
        for (int j = 0; j < A.length; j++) {
            sum += A[i][j];
        }
        return sum;
    }

    //Tính tổng các phần tử trên cột j của ma trận vuông A
    public static int sumOfColumn(int[][] A, int j) {
        int sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum += A[i][j];
        }
        return sum;
    }

    //Tính tổng các phần tử trên đường chéo chính của ma trận vuông A
    public static int sumOfPrimaryDiagonal(int[][] A) {
        int sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum += A[i][i];
        }
        return sum;
    }

    //Tính tổng các phần tử trên đường chéo phụ của ma trận vuông A
    public static int sumOfSecondaryDiagonal(int[][] A) {
        int sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum += A[i][A.length - i - 1];
        }
        return sum;
    }
}
