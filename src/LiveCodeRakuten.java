import java.util.Arrays;

public class LiveCodeRakuten {

    public static String solution(String input) {
        StringBuilder res = new StringBuilder();
        int i = 0;

        if ('A' <= input.charAt(i) && 'Z' >= input.charAt(i)) {
            res.append(input.charAt(i));
        }

        for (; i < input.length() - 1; i++) {
            int nextIndex = i + 1;
            if (input.charAt(i) == ' ' && 'A' <= input.charAt(nextIndex) && 'Z' >= input.charAt(nextIndex)) {
                res.append(input.charAt(nextIndex));
            }
        }

        return res.toString();
    }

    public static String solution2(String input) {
        return Arrays.stream(input.split(" "))
                .map(s -> s.length() > 0 && 'A' <= input.charAt(0) && 'Z' >= input.charAt(0) ? s.charAt(0) : null)
                .collect(StringBuilder::new, (stringBuilder, character) -> {
                    if (character != null)
                        stringBuilder.appendCodePoint(character);
                }, StringBuilder::append)
                .toString();
    }

    public static void main(String[] args) {
        String word = "    Happy _char New  notYear   Year  "; // HNY
        System.out.println(solution(word));
    }
}


