import java.util.Stack;

public class ShopeeTest {

    //Given two strings S and T, return if they are equal when both are typed into empty text editors. # means a backspace character.

    public static boolean solution(String s1, String s2) {
        return cleanSharp(s1).equals(cleanSharp(s2));
    }

    static String cleanSharp(String s) {
        Stack<Character> ans = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c != '#') {
                ans.push(c);
            } else if (!ans.empty()) {
                ans.pop();
            }
        }
        return String.valueOf(ans);
    }

    public static void main(String[] args) {
        System.out.println(solution("abc##", "ab#c#"));
    }
}
